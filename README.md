Mary Cheatham King Real Estate is the #1 real estate group on the Crystal Coast! MCKREs real estate agents and staff are local experts with deep roots in the Carteret County community who believe that real estate is not just about transactions, its about relationships. If you are looking for investment properties or homes for sale in Morehead City, Emerald Isle, Beaufort, or the Atlantic Beach area, contact our real estate agents today!

Website: https://www.marycheathamking.com
